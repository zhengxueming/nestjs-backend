import { InMemoryDBService } from '@nestjs-addons/in-memory-db';
import { Body, Controller, Get, HttpException, HttpStatus, Post, Req, Request, UploadedFile, UseInterceptors } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { imageFileFilter, editFilename } from './file-helper';
import { UserProfileEntity } from '../entity/user-profile-entity.interface';
import { ApiBearerAuth, ApiBody, ApiConsumes } from '@nestjs/swagger';

@Controller('api')
export class ApiController {

  constructor(private readonly userProfileService: InMemoryDBService<UserProfileEntity>) { }

  @Get('me')
  @ApiBearerAuth()
  getCurrentUser(@Req() request: Request) {
    return request['user'];
  }

  @Post('upload')
  @ApiBearerAuth()
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  @UseInterceptors(
    FileInterceptor('file', {
      fileFilter: imageFileFilter,
      storage: diskStorage({
        destination: './avatars',
        filename: editFilename
      })
    }))
  uploadProfilePicture(@Req() req: Request, @UploadedFile() file: any) {
    if (!file) {
      throw new HttpException("file is required", HttpStatus.BAD_REQUEST);
    }
    const foundProfiles = this.userProfileService.query(r => r.uid === req['user']['uid'])
    if (foundProfiles.length > 0) {
      let profile = foundProfiles[0];
      profile.filename = file.filename;
      this.userProfileService.update(profile);
      return profile;
    } else {
      const newProfile = this.userProfileService.create(
        {
          uid: req['user']['uid'],
          filename: file.filename,
        }
      );
      return newProfile;
    }
  }
}
