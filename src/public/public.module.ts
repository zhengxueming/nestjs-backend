import { InMemoryDBModule } from '@nestjs-addons/in-memory-db';
import { Module } from '@nestjs/common';
import { PublicController } from './public.controller';

@Module({
  imports: [
    InMemoryDBModule.forRoot(),
  ],
  controllers: [PublicController]
})
export class PublicModule {}
