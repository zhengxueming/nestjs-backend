import { HttpException, HttpStatus, Injectable, NestMiddleware } from '@nestjs/common';
import { AuthService } from './auth.service';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly authService: AuthService,
  ) { }

  use(req: any, res: any, next: () => void) {
    const token = req.headers.authorization;
    if (token != null && token != '') {
      this.authService.verifyIdToken(token.replace('Bearer ', ''))
        .then(decodedToken => {
          this.authService.getUser(decodedToken.uid)
          .then(u => {
            req.user = u;
            next();
          });
        })
        .catch(error => {
          console.error(error);
          res.status(HttpStatus.UNAUTHORIZED).json({
            message: error.message,
          })
        });
    } else {
      throw new HttpException("Unauthorized", HttpStatus.UNAUTHORIZED);
    }
  }
}
